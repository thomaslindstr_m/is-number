// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var isNumber = require('./index.js');
    var assert = require('assert');

    describe('is-number', function() {
        it('should return false for empty strings', function () {
            assert.equal(isNumber(''), false);
        });

        it('should return false for strings with content', function () {
            assert.equal(isNumber('hello'), false);
        });

        it('should return true for numbers', function () {
            assert.equal(isNumber(1), true);
        });

        it('should return true for negative numbers', function () {
            assert.equal(isNumber(-1), true);
        });

        it('should return true for 0', function () {
            assert.equal(isNumber(0), true);
        });

        it('should return false for objects', function () {
            assert.equal(isNumber({}), false);
        });

        it('should return false for functions', function () {
            assert.equal(isNumber(function () {}), false);
        });

        it('should return false for arrays', function () {
            assert.equal(isNumber([]), false);
        });

        it('should return false for undefined', function () {
            assert.equal(isNumber(undefined), false);
        });

        it('should return false for null', function () {
            assert.equal(isNumber(null), false);
        });

        it('should return false for false', function () {
            assert.equal(isNumber(false), false);
        });

        it('should return false for true', function () {
            assert.equal(isNumber(true), false);
        });
    });
