// ---------------------------------------------------------------------------
//  is-number
//
//  number type checker
//  - variable: Variable to be checked
// ---------------------------------------------------------------------------

    function isNumber(variable) {
        return !isNaN(parseFloat(variable)) && isFinite(variable);
    }

    module.exports = isNumber;
